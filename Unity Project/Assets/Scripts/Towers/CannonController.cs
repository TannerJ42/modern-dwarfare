﻿using UnityEngine;
using System.Collections;

public class CannonController : TowerController 
{
	public GameObject projectile;

    protected int damage = 0;
	
	// Update is called once per frame
	protected override void onUpdate() 
	{
		if (attackTimer <= 0 &&
		    currentTarget != null)
		{
			anim.SetTrigger("Attack");
			Attack(currentTarget.transform);
		}
	}

	public void FinishAttacking()
	{

	}

	public void Attack(Transform targetTransform)
	{
		attackTimer = attackInterval;
		
		Vector2 direction = (Vector2)targetTransform.position - (Vector2)transform.position;
		float angle = Mathf.Atan2 (direction.y, direction.x) * Mathf.Rad2Deg;
		Quaternion rotation = Quaternion.AngleAxis (angle, Vector3.forward);
		
		ProjectileController p = ((GameObject)Instantiate(projectile, transform.position, rotation)).GetComponent<ProjectileController>();
        p.firer = this.gameObject;
		p.SetDirection(direction);
        p.damage = this.damage;
	}

    public override void AssignStats()
    {
        base.AssignStats();

        damage = damageForRank[currentRank];
    }
}
