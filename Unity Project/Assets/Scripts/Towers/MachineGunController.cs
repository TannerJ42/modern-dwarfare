﻿using UnityEngine;
using System.Collections;

public class MachineGunController : TowerController 
{

	int damage;

    protected override void onUpdate()
    {
        if (attackTimer <= 0
		    && currentTarget != null)
		{
			MachineGunDamage();
		}

		anim.SetBool("Attacking", currentTarget != null);
    }

	void MachineGunDamage()
	{
		if (currentTarget == null)
		{
			return;
		}

		attackTimer = attackInterval;

        NewEnemyController ec = currentTarget.GetComponent<NewEnemyController>();

		if (ec == null)
		{
			return;
		}

		ec.TakeDamage(damage, ec.gameObject.transform.position - transform.position, this.gameObject);
	}

    public override void AssignStats()
    {
        base.AssignStats();

        damage = damageForRank[currentRank];
    }
}
