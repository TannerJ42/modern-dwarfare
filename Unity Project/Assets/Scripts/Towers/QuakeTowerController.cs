﻿using UnityEngine;
using System.Collections;

public class QuakeTowerController : TowerController 
{
    public GameObject quake;

    protected int damage = 0;

    // Update is called once per frame
    protected override void onUpdate()
    {
        if (attackTimer <= 0 &&
            currentTarget != null)
        {
            anim.SetTrigger("Attack");
            Attack();
        }
    }

    void Attack()
    {
        attackTimer = attackInterval;

        QuakeController q = ((GameObject)Instantiate(quake, transform.position, transform.rotation)).GetComponent<QuakeController>();
        q.attacker = this.gameObject;
        q.setDamage(damage);
    }

    public override void AssignStats()
    {
        base.AssignStats();
        damage = damageForRank[currentRank];
    }
}
