﻿using UnityEngine;
using System.Collections;

public class BuildingAnimationManager : MonoBehaviour 
{
	public GameObject building;

	public float buildTime = 5f;
	float timer;

	// Use this for initialization
	void Start () 
	{
		timer = buildTime;
	}
	
	// Update is called once per frame
	void Update () 
	{
		timer -= Time.deltaTime;
		if (timer <= 0)
		{
			Debug.Log ("Done!");

			FinishBuilding();
		}
	}

	public void FinishBuilding()
	{
		if (building != null)
		{
			building.SetActive(true);
		}
		Debug.Log ("Done!");

		Destroy (this.transform.parent.gameObject);
		Destroy (this.gameObject);
	}
}
