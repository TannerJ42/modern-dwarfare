﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class TowerController : MonoBehaviour 
{
    protected List<NewEnemyController> enemies;

	public float attackTimer = 0;
	public float attackInterval = 1f;

	//public GameObject projectile;

	public int maxHP = 100;
	public int currentHP;

	public GameObject currentTarget;

    public CircleCollider2D rangeCollider;

	protected Animator anim;

	public HealthBarController healthBar;

    protected int currentRank = 0;
    protected float currentXP = 0;

    public int[] xpForRank;
    public int[] damageForRank;
    public float[] rateOfFireForRank;
    public float[] rangeForRankUp;
    public int[] HPCapForRank;

	public bool isAlive
	{
		get
		{
			return currentHP > 0;
		}
	}

	// Use this for initialization
	void Start()
	{
        enemies = new List<NewEnemyController>();
		currentHP = maxHP;
		anim = GetComponentInChildren<Animator>();

        AssignStats();
	}
	
	// Update is called once per frame
	void Update () 
	{
		FindTarget();

		if (currentTarget != null)
		{
			FaceTarget((Vector2)currentTarget.transform.position);
		}

        if (currentRank < xpForRank.Length - 1)
        {
            currentXP += Time.deltaTime;

            if (currentXP >= xpForRank[currentRank])
            {
                RankUp();
            }
        }

        onUpdate();
	}

    protected virtual void onUpdate()
    {

    }

	void FixedUpdate()
	{
		if (!isAlive)
		{
			return;
		}

		if (attackTimer > 0)
		{
			attackTimer -= Time.fixedDeltaTime;
		}

//		if (attackTimer <= 0)
//		{
//			TryToAttack();
//		}
	}

	void OnTriggerEnter2D(Collider2D c)
	{
		if (c.gameObject == null)
			return;

		if (c.tag != "Enemy" ||
		    c.isTrigger ||
		    enemies == null)
		{
			return;
		}

        NewEnemyController controller = c.GetComponent<NewEnemyController>();

		if (!enemies.Contains(controller))
		{
			enemies.Add(controller);
		}
	}

	void OnTriggerExit2D(Collider2D c)
	{
		if (c.gameObject == null)
			return;

		if (c.tag != "Enemy" ||
            c.isTrigger ||
		    enemies == null)
		{
			return;
		}

        NewEnemyController controller = c.GetComponent<NewEnemyController>();

		if (enemies.Contains(controller))
		{
			enemies.Remove(controller);
		}
	}

	void FindTarget()
	{
        NewEnemyController target = null;
		float closest = float.MaxValue;

		for (int i = enemies.Count - 1; i >= 0; i--)
		{
			if (enemies[i] == null ||
			    !enemies[i].isAlive)
			{
				enemies.RemoveAt(i);
			}
			else
			{
				float distance = Vector2.Distance(transform.position, enemies[i].transform.position);

				if (distance < closest &&
				    enemies[i].isAlive)
				{
					target = enemies[i];
					closest = distance;
				}
			}
		}

		if (target != null)
			currentTarget = target.gameObject;
	}

	void FaceTarget(Vector2 faceTarget)
	{
		if (faceTarget == Vector2.zero)
			return;

		Vector2 direction = faceTarget - (Vector2)transform.position;
		float angle = Mathf.Atan2 (direction.y, direction.x) * Mathf.Rad2Deg;
		angle += 270;
		transform.rotation = Quaternion.AngleAxis (angle, Vector3.forward);
	}

	public void takeDamage(int damage, Vector2 location)
	{
		if (this.name == "Base")
		{
			CameraShake.Shake();
		}

		if (!isAlive)
		{
			return;
		}

		currentHP -= damage;

		if (healthBar != null)
		{
			healthBar.SetDamage(currentHP/(float)maxHP);
		}
		
		if (currentHP <= 0)
		{
			Destroy(this.gameObject);
		}
	}

    public void HealDamage(int healPower)
    {
        if (!isAlive)
        {
            return;
        }

        currentHP += healPower;
        if (currentHP > maxHP)
        {
            currentHP = maxHP;
        }

        if (healthBar != null)
        {
            healthBar.SetDamage(currentHP / (float)maxHP);
        }
    }

    public void GainXP(int xp)
    {
        //currentXP += xp;

        //if (currentRank < xpForRank.Length - 1 &&
        //    currentXP >= xpForRank[currentRank])
        //{
        //    RankUp();
        //}
    }

    public virtual void RankUp()
    {
        Debug.Log("Rank up!");
        currentXP -= xpForRank[currentRank];
        currentRank += 1;
        AssignStats();
    }

    public virtual void AssignStats()
    {

        float ratio = currentHP / (float)maxHP;
        maxHP = HPCapForRank[currentRank];
        currentHP = (int)(maxHP * ratio);

        if (rangeCollider)
        {
            rangeCollider.radius = rangeForRankUp[currentRank];
        }

        attackInterval = rateOfFireForRank[currentRank];

        if (attackTimer > attackInterval)
        {
            attackTimer = attackInterval;
        }
    }
}
