﻿using UnityEngine;
using System.Collections;

public class Coins : MonoBehaviour 
{
	public static int Amount = 60;
	public static int Tower1Cost = 15; // machine gun
	public static int Tower2Cost = 20; // cannon
	public static int Tower3Cost = 30; // sniper
    public static int Tower4Cost = 40; // quake

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{

	}

	public static void Reset()
	{
		Amount = 60;
	}

	public static void AddCoins(int amount)
	{
		Amount += amount;
	}

	public static void Create()
	{

	}
}
