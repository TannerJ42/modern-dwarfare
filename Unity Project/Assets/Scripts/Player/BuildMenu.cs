﻿using UnityEngine;
using System.Collections;

public class BuildMenu : MonoBehaviour 
{
	public TextMesh tooltip;

	public GameObject MGIcon;
	public TextMesh MGCostText;
	public int MGCost = 15;
	public string machineGunText = "Gun Tower:\n  Fires rapidly.";

	public GameObject CIcon;
	public TextMesh CCostText;
	public int CCost = 20;
	public string cannonText = "Cannon Tower:\n  Durable, slow, long range.";

	public GameObject TIcon;
	public TextMesh TCostText;
	public int TCost = 10;
	public string teleportText = "Teleport Tower:\n  Click tower to teleport.";

	// Use this for initialization
	void Start() 
	{
		MGCostText.text = Coins.Tower1Cost.ToString();
		CCostText.text = Coins.Tower2Cost.ToString();
		TCostText.text = Coins.Tower3Cost.ToString();
	}
	
	// Update is called once per frame
	void Update() 
	{
		CheckIcon(MGIcon, MGCostText, MGCost);
		CheckIcon(CIcon, CCostText, CCost);
		CheckIcon(TIcon, TCostText, TCost);
	}

	void CheckIcon(GameObject icon, TextMesh costText, int cost)
	{
		if (!icon.renderer.enabled)
		{
			if (cost <= Coins.Amount)
			{
				icon.renderer.enabled = true;
				costText.color = Color.white;
			}
		}
		else
		{
			if (cost > Coins.Amount)
			{
				icon.renderer.enabled = false;
				costText.color = Color.red;
			}
		}
	}

	public void HoverOver(string type)
	{
		switch (type)
		{
			case "MG":
				tooltip.text = machineGunText;
				break;
			case "C":
				tooltip.text = cannonText;
				break;
			case "T":
				tooltip.text = teleportText;
				break;
			default:
				break;
		}
	}

	public void TryToBuy(string type)
	{
		switch (type)
		{
			case "MG":
				break;
			case "C":
				break;
			case "T":
				break;
			default:
				break;
		}
	}
}
