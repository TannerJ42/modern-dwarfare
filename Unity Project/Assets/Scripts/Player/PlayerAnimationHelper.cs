﻿using UnityEngine;
using System.Collections;

public class PlayerAnimationHelper : MonoBehaviour 
{
	PlayerController pc;

	// Use this for initialization
	void Start () 
	{
		pc = transform.parent.GetComponent<PlayerController>();
	}

	public void FinishAttacking()
	{
		pc.FinishAttacking();
	}

    public void Teleport()
    {
        pc.Teleport();
    }

    public void FinishTeleporting()
    {
        pc.FinishTeleporting();
    }
}
