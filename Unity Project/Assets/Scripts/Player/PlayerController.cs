﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour 
{
	public float speed = 5f;

	//Transform myTransform;

	float attackTimer = 0;
	public float attackSpeed = 1f;

	Animator anim;

	public AxeController axe;

    public bool faceMouse = false;

    public GameObject teleportRune;
    public float teleportCooldown = 5f;
    float teleportTimer = 0;

    State state = State.Idle;

    bool wantsToTeleport = false;

	// Use this for initialization
	void Start() 
	{
		//myTransform = gameObject.GetComponent<Transform>();
		anim = gameObject.GetComponentInChildren<Animator>();

		attackTimer = attackSpeed;
        teleportTimer = teleportCooldown;

		Coins.Reset();
	}
	
	// Update is called once per frame
	void Update()
	{
        if (wantsToTeleport &&
            state == State.Idle)
        {
            StartTeleporting();
            return;
        }

        if (faceMouse)
        {
            FaceMouse();
        }
		CheckForAttack();
        CheckForTeleport();
        CheckForMoveTeleporter();
	}

	void FixedUpdate()
	{
		if (attackTimer > 0)
		{
			attackTimer -= Time.fixedDeltaTime;
		}
        if (teleportTimer > 0)
        {
            teleportTimer -= Time.fixedDeltaTime;
        }
		Move();
	}

	void Move()
	{
        if (state == State.Teleporting)
        {
            return;
        }

		float stepSpeed = speed * Time.fixedDeltaTime;
		
		Vector2 direction = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

		direction.Normalize();

		Vector2 movement = (Vector2)transform.position + direction * stepSpeed;
		anim.SetFloat("speed", direction.magnitude);
		rigidbody2D.MovePosition(movement);

        if (!faceMouse &&
            direction != Vector2.zero)
        {
            FaceLookDirection(direction);
        }
	}

    void FaceLookDirection(Vector2 direction)
    {
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        angle += 270;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

	void FaceMouse()
	{
		Vector2 mousePosition =  Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
		Vector2 direction = mousePosition - (Vector2)transform.position;
		float angle = Mathf.Atan2 (direction.y, direction.x) * Mathf.Rad2Deg;
		angle += 270;
		transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
	}

	public void FinishAttacking()
	{
        state = State.Idle;
		attackTimer = attackSpeed;
		axe.EndSwing();
	}

	void CheckForAttack()
	{
		if (state != State.Idle ||
		    attackTimer > 0)
		{
			return;
		}

		if (Input.GetAxis("Attack") == 1)
		{
			axe.StartSwing();
            state = State.Attacking;
			anim.SetTrigger("attack");
		}
	}

    void CheckForTeleport()
    {
        if (teleportRune == null ||
            teleportTimer > 0 ||
            state == State.Teleporting)
        {
            return;
        }

        if (Input.GetAxis("Teleport") == 1)
        {
            wantsToTeleport = true;
        }
    }

    void CheckForMoveTeleporter()
    {
        if (state == State.Teleporting)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.E) ||
            Input.GetMouseButtonDown(2))
        {
            ChangeTeleportPosition();
        }
    }

    public void Teleport()
    {
        Vector3 position = teleportRune.transform.position;
        position.z = transform.position.z;
        transform.position = position;
    }

    public void StartTeleporting()
    {
        wantsToTeleport = false;
        state = State.Teleporting;
        
        anim.SetTrigger("teleport");
    }

    public void FinishTeleporting()
    {
        state = State.Idle;
        teleportTimer = teleportCooldown;
    }

    public void ChangeTeleportPosition()
    {
        if (teleportRune != null)
        {
            Vector3 position = transform.position;
            position.z = teleportRune.transform.position.z;
            teleportRune.transform.position = position;
        }
    }

    public enum State
    {
        Idle,
        Attacking,
        Teleporting,
    }
}