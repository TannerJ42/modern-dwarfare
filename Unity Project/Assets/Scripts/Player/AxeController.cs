﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AxeController : MonoBehaviour 
{
	public int damage = 50;
    public int healPower = 10;

	bool swinging = false;

	List<GameObject> enemiesHit;
    List<GameObject> towersHealed;

	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}

	public void StartSwing()
	{
		swinging = true;
		towersHealed = new List<GameObject>();
		enemiesHit = new List<GameObject>();
	}

	public void EndSwing()
	{
		swinging = false;
		enemiesHit = null;
	}

    void HealDamage(Collider2D c)
    {
        if (towersHealed.Contains(c.gameObject))
        {
            return;
        }

        towersHealed.Add(c.gameObject);
        TowerController t = c.gameObject.GetComponent<TowerController>();
        t.HealDamage(healPower);
    }

    void DoDamage(Collider2D c)
    {
        if (enemiesHit.Contains(c.gameObject))
        {
            return;
        }

        enemiesHit.Add(c.gameObject);
        NewEnemyController e = c.gameObject.GetComponent<NewEnemyController>();
        e.TakeDamage(damage, this.transform.position, this.gameObject);
    }

    void OnTriggerStay2D(Collider2D c)
    {
        if (c == null ||
            c.gameObject == null)
            return;

        if (swinging)
        {
            if (c.tag == "Enemy" &&
                !c.isTrigger)
            {
                DoDamage(c);
            }
            else if (c.tag == "Tower")
            {
                HealDamage(c);
            }
        }
    }

	void OnTriggerEnter2D(Collider2D c)
	{
		if (c == null ||
            c.gameObject == null)
			return;

        if (swinging)
        {
            if (c.tag == "Enemy" &&
                !c.isTrigger)
            {
                DoDamage(c);
            }
            else if (c.tag == "Tower")
            {
                HealDamage(c);
            }
        }
	}
}
