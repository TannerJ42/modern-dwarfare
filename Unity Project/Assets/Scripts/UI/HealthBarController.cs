﻿using UnityEngine;
using System.Collections;

public class HealthBarController : MonoBehaviour 
{
	public GameObject green;
	public GameObject red;

	Quaternion rotation;

	Vector3 offset;

    int maxHP;

	// Use this for initialization
	void Start () 
	{
		this.renderer.enabled = false;
		green.renderer.enabled = false;
		red.renderer.enabled = false;
		this.rotation = transform.rotation;
		offset = transform.localPosition;
	}

	public void SetDamage(float ratio)
	{
        if (ratio == 1 &&
            renderer.enabled)
        {
            renderer.enabled = false;
            green.renderer.enabled = false;
            red.renderer.enabled = false;
        }

		if (ratio < 1 &&
		    !renderer.enabled)
		{
			this.renderer.enabled = true;
			green.renderer.enabled = true;
			red.renderer.enabled = true;
		}

		Vector3 barScale = green.transform.localScale;

		barScale.x = ratio;

		green.transform.localScale = barScale;
	}

	void LateUpdate()
	{
		this.transform.rotation = rotation;
		this.transform.position = transform.parent.position + offset;
	}
}
