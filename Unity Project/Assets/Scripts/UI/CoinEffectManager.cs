﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CoinEffectManager : MonoBehaviour 
{
	static CoinEffectManager Manager;

	public List<DeathCoin> deathCoins;

	public GameObject deathCoinPrefab;

	public int initialCoins = 5;

	// Use this for initialization
	void Start () 
	{
		Manager = this;

		deathCoins = new List<DeathCoin>();

		for (int i = 0; i < initialCoins; i++)
		{
			deathCoins.Add(CreateCoin());
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	DeathCoin CreateCoin()
	{
		DeathCoin dc = ((GameObject)Instantiate (deathCoinPrefab)).GetComponent<DeathCoin>();
		dc.transform.parent = this.gameObject.transform;
		return dc;
	}

	public void placeCoins(Vector2 location, int amount)
	{
		for (int i = 0; i <= deathCoins.Count - 1; i++)
		{
			if (!deathCoins[i].inUse)
			{
				deathCoins[i].Appear(location, amount);
				return;
			}
		}

		DeathCoin coin = CreateCoin();
		coin.Appear(location, amount);
		deathCoins.Add(coin);

	}

	public static void AddCoinEffect(Vector2 location, int amount)
	{
		if (Manager == null)
		{
			return;
		}

		Manager.placeCoins(location, amount);
	}
}
