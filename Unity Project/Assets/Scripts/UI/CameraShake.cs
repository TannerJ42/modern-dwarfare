﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour 
{
	Animator anim;

	static int shakes = 0;

	public string[] names;

	float timer;
	public float timeBetweenShakes = 3f;

	// Use this for initialization
	void Start() 
	{
		anim = GetComponent<Animator>();	
	}
	
	// Update is called once per frame
	void Update() 
	{
		timer -= Time.deltaTime;

		if (timer <= 0)
		{
			if (shakes > 0)
			{
				ShakeRandomly();
				shakes = 0;
				timer = timeBetweenShakes;
			}
		}
		else
		{
			shakes = 0;
		}
	}

	public static void Shake()
	{
		shakes += 1;
	}

	void ShakeRandomly()
	{
		anim.SetTrigger(names[Random.Range(1, names.Length)]);
	}
}
