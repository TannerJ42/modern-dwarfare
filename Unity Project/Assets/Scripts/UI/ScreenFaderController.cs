﻿using UnityEngine;
using System.Collections;

public class ScreenFaderController : MonoBehaviour 
{
	public static bool win = true;
	Animator anim;

	// Use this for initialization
	void Start () 
	{
		anim = GetComponent<Animator>();
        win = true;
	}

	public static void Lose()
	{
		win = false;
	}

	public static void Win()
	{
		win = true;
	}

	public void Finish()
	{
		if (win)
			Application.LoadLevel("Win");
		else
			Application.LoadLevel("Lose");
	}
}
