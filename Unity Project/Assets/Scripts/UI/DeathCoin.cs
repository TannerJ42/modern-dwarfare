﻿using UnityEngine;
using System.Collections;

public class DeathCoin : MonoBehaviour 
{
	Animator anim;
	public TextMesh text;
	public SpriteRenderer sprite;

	public float yOffset = 0.2f;

	public bool inUse = false;


	// Use this for initialization
	void Start() 
	{
		if (!anim)
		{
			anim = GetComponent<Animator> ();
		}

		if (!inUse)
		{
			Disappear();
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void Appear(Vector2 location, int amount)
	{
		transform.position = new Vector3(location.x, location.y + yOffset, transform.position.z);
		text.text = amount.ToString();
		text.renderer.enabled = true;
		sprite.renderer.enabled = true;

		if (!anim)
		{
			anim = GetComponent<Animator>();
		}

		anim.SetTrigger("Collect");

		inUse = true;
	}

	public void Disappear()
	{
		text.renderer.enabled = false;
		sprite.renderer.enabled = false;
		inUse = false;
	}
}
