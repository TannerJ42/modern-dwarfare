﻿using UnityEngine;
using System.Collections;

public class StartScript : MonoBehaviour 
{
	public float timer = 1f;
	public string nextScene = "Gameplay";

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (timer > 0)
			timer -= Time.deltaTime;

		if (timer <= 0 &&
		    Input.GetMouseButtonDown(0))
		{
			Application.LoadLevel(nextScene);
		}
	}
}
