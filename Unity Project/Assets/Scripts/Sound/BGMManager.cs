﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BGMManager : MonoBehaviour 
{
	public AudioClip startingBGM;
	public AudioClip nearDeathBGM;
	public AudioSource source;

	TowerController[] baseTowers;

	bool danger = false;
	public float changePercent = 0.2f;

	// Use this for initialization
	void Start () 
	{
        source.clip = startingBGM;

        source.Play();

        GameObject[] towers = GameObject.FindGameObjectsWithTag("Tower");

        List<TowerController> bases = new List<TowerController>();

        for (int i = 0; i <= towers.Length - 1; i++)
        {
            TowerController tc = towers[i].GetComponent<TowerController>();
            if (tc.gameObject.name == "Base")
            {
                bases.Add(tc);
            }
        }

        baseTowers = bases.ToArray();


	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!danger &&
            GettingDangerous)
		{
			danger = true;
			source.Stop();
			source.clip = nearDeathBGM;
			source.Play();
		}
	}

    bool GettingDangerous
    {
        get
        {
            if (baseTowers == null)
            {
                return false;
            }

            foreach (TowerController tc in baseTowers)
            {
                if (tc.currentHP / (float)tc.maxHP < changePercent)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
