﻿using UnityEngine;
using System.Collections;

public class ProjectileController : MonoBehaviour 
{
	public float speed = 5f;
	public int damage = 10;
	public string targetTag = "Enemy";

	Vector2 direction = Vector2.up;

    public GameObject firer;

    public float secondsBeforeDestroyed = 2f;

	// Use this for initialization
	void Start () 
	{
		Destroy(this.gameObject, secondsBeforeDestroyed);
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}


	void FixedUpdate()
	{
		Vector2 movement = (Vector2)transform.position + direction * speed * Time.fixedDeltaTime;

		transform.position = new Vector3(movement.x, movement.y, transform.position.z);
	}

	public void SetDirection(Vector2 newDirection)
	{
		direction.Normalize();

		direction = newDirection;

        float angle = Vector2.Angle(Vector2.up, direction);
        Quaternion quat = Quaternion.identity;
        quat.eulerAngles = new Vector3(0, 0, angle);
        transform.rotation = quat;
	}

	void OnTriggerEnter2D(Collider2D c)
	{
		if (c == null ||
		    c.tag != targetTag ||
		    c.isTrigger)
		{
			return;
		}

        onDoDamage(c);

        onHitTarget(c.tag);
	}

    protected virtual void onDoDamage(Collider2D c)
    {
        Debug.Log("Dealing damage.");
        if (c.tag == "Enemy")
        {
            NewEnemyController e = c.GetComponent<NewEnemyController>();
            e.TakeDamage(damage, (Vector2)c.transform.position, firer);
        }

        if (c.tag == "Tower")
        {
            TowerController t = c.GetComponent<TowerController>();
            t.takeDamage(damage, (Vector2)c.transform.position);
        }
    }

    protected virtual void onHitTarget(string tag)
    {
        Destroy(this.gameObject);
    }
}
