﻿using UnityEngine;
using System.Collections;

public class CannonballController : ProjectileController 
{
    public GameObject explosionPrefab;
    public float damageMultiplier = 1f; // multiply tower 

    protected override void onHitTarget(string tag)
    {
        explode();
        base.onHitTarget(tag);
    }

    protected override void onDoDamage(Collider2D c)
    {
        // explode instead.
    }

    void explode()
    {
        GameObject exp = (GameObject)Instantiate(explosionPrefab, transform.position, transform.rotation);

        // set damage on explosion.
        ExplosionController e = exp.GetComponent<ExplosionController>();
        e.setDamage((int)(damage * damageMultiplier));
        e.attacker = firer;
    }
}
