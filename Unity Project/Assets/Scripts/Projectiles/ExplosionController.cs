﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ExplosionController : MonoBehaviour 
{
    int damage = 1000;

    List<GameObject> enemiesInRange;

    bool dealtDamage = false;

    public GameObject attacker;

    void Start()
    {
        enemiesInRange = new List<GameObject>();
    }

    public void setDamage(int damage)
    {
        this.damage = damage;
    }

    public void DealDamage()
    {
        foreach (GameObject g in enemiesInRange)
        {
            if (g != null)
            {
                NewEnemyController e = g.GetComponent<NewEnemyController>();
                e.TakeDamage(damage, transform.position, attacker);
            }
        }

        dealtDamage = true;
    }

    public void Explode()
    {
        Destroy(this.gameObject);
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.gameObject == null)
        {
            return;
        }

        if (c.tag == "Enemy" &&
            !c.isTrigger)
        {
            if (!enemiesInRange.Contains(c.gameObject))
            {
                enemiesInRange.Add(c.gameObject);
            }

            //EnemyController e = c.gameObject.GetComponent<EnemyController>();
            //if (!enemiesInRange.Contains(e))
            //{
            //    enemiesInRange.Add(e);
            //}
        }
    }
}
