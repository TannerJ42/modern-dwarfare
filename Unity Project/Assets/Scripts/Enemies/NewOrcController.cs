﻿using UnityEngine;
using System.Collections;

public class NewOrcController : NewEnemyController 
{
    public GameObject projectile;

    public override void onAttackStart()
    {
        base.onAttackStart();
    }

    public override void onUseAttack()
    {
        if (currentTarget == null)
        {
            return;
        }

        Vector2 direction = (Vector2)currentTarget.transform.position - (Vector2)transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        ProjectileController p = ((GameObject)Instantiate(projectile, transform.position, rotation)).GetComponent<ProjectileController>();
        p.SetDirection(direction);

        base.onUseAttack();
    }
}
