﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyController : MonoBehaviour 
{
	public WaypointController currentWaypoint;
	public float speed = 10f;

	Transform myTransform;

	public int maxHP = 100;
	int currentHP;

	public int coinValue = 10;
	
	public State state = State.Walking;

	Animator anim;

	//public GameObject projectile;

	public GameObject currentTarget;

	List<TowerController> towers;

	public HealthBarController healthBar;

	public float attackTimer;
	public float attackCooldown = 5f;

	public GameObject coinPrefab;

	// Use this for initialization
	void Start() 
	{
		myTransform = this.gameObject.transform;
		currentHP = maxHP;

		anim = GetComponentInChildren<Animator>();

		towers = new List<TowerController>();
	}

	bool canAttack
	{
		get
		{
			return (state == State.Walking &&
			        attackTimer <= 0 &&
			        isAlive);
		}
	}

	public bool isAlive
	{
		get
		{
			return currentHP > 0;
		}
	}

	// Update is called once per frame
	void Update() 
	{
		if (attackTimer > 0)
		{
			attackTimer -= Time.deltaTime;
		}
		
		if (attackTimer <= 0 &&
		    state == State.Walking)
		{
			FindTarget();
			if (currentTarget != null)
				StartAttacking();
		}
		
		if (state == State.Attacking &&
		    currentTarget != null)
		{
			FaceTarget((Vector2)currentTarget.transform.position);
		}
	}

	void FixedUpdate()
	{
		switch (state)
		{
		case State.Walking:
			Move();
			UpdateWaypoint();
			break;
		case State.Attacking:
			break;
		default:
			break;
		}
	}

	void Move()
	{
		if (currentWaypoint == null)
		{
			return;
		}

		float stepSpeed = speed * Time.fixedDeltaTime;

		Vector2 direction = Vector2.MoveTowards(myTransform.position, currentWaypoint.GetCoordinates(), stepSpeed);

		//transform.position = new Vector3(direction.x, direction.y, myTransform.position.z);

		rigidbody2D.MovePosition(new Vector3(direction.x, direction.y, myTransform.position.z));

		FaceTarget(currentWaypoint.GetCoordinates());
	}

	void FaceTarget(Vector2 faceTarget)
	{
		if (faceTarget == Vector2.zero)
		{
			return;
		}

		Vector2 direction = faceTarget - (Vector2)myTransform.position;
		float angle = Mathf.Atan2 (direction.y, direction.x) * Mathf.Rad2Deg;
		transform.rotation = Quaternion.AngleAxis (angle, Vector3.forward);
	}

	void FindTarget()
	{
		currentTarget = null;
		TowerController target = null;
		float closest = float.MaxValue;
		
		for(int i = towers.Count - 1; i >= 0; i--)
		{
			if (towers[i] == null ||
			    !towers[i].isAlive)
			{
				towers.RemoveAt(i);
			}
			else
			{
				float distance = Vector2.Distance(transform.position, towers[i].transform.position);
				
				if (distance < closest &&
				    towers[i].isAlive)
				{
					target = towers[i];
					closest = distance;
				}
			}
		}
		
		if (target != null)
			currentTarget = target.gameObject;
	}

	public void StartAttacking()
	{
		anim.SetTrigger("Attack");
		state = State.Attacking;
	}
	
	public void FinishAttacking()
	{
		currentTarget = null;
		state = State.Walking;
	}

	void UpdateWaypoint()
	{
		if (currentWaypoint == null)
		{
			return;
		}

		// check for waypoint change
		if (Vector2.Distance(currentWaypoint.GetCoordinates(), this.myTransform.position) <= 0.1f)
		{
			currentWaypoint = currentWaypoint.GetNextWaypoint();
		}
	}

	public void takeDamage(int damage, Vector2 point)
	{
		if (!isAlive)
		{
			return;
		}
		
		currentHP -= damage;

		if (healthBar != null)
		{
			healthBar.SetDamage(currentHP/(float)maxHP);
		}

		if (currentHP <= 0)
		{
			CoinEffectManager.AddCoinEffect((Vector2)transform.position, coinValue);			
			Coins.AddCoins(coinValue);
			Destroy(this.gameObject);
		}
	}

//	public void Attack()
//	{
//		if (currentTarget == null)
//		{
//			return;
//		}
//
//		Vector2 direction = (Vector2)currentTarget.transform.position - (Vector2)transform.position;
//		float angle = Mathf.Atan2 (direction.y, direction.x) * Mathf.Rad2Deg;
//		Quaternion rotation = Quaternion.AngleAxis (angle, Vector3.forward);
//		
//		ProjectileController p = ((GameObject)Instantiate(projectile, transform.position, rotation)).GetComponent<ProjectileController>();
//		p.SetDirection(direction);
//	}

	void OnCollisionEnter2D(Collision2D c)
	{
		if (c.gameObject == null)
			return;

		if (c.gameObject.tag == "Enemy")
		{
			Physics2D.IgnoreCollision(c.collider, this.collider2D);
		}
	}

	void OnTriggerEnter2D(Collider2D c)
	{
		if (c.gameObject == null)
			return;

		if (c.tag == "Enemy")
		{
			Physics2D.IgnoreCollision(c, this.collider2D);
		}

		if (c.tag != "Tower" ||
		    c.isTrigger)
		{
			return;
		}

		TowerController controller = c.GetComponent<TowerController>();

		if (controller == null ||
		    towers == null)
			return;

		if (!towers.Contains(controller))
		{
			towers.Add(controller);
		}
	}

	void OnTriggerStay2D(Collider2D c)
	{
		if (c.gameObject == null)
			return;
		
		if (c.tag == "Enemy")
		{
			Physics2D.IgnoreCollision(c, this.collider2D);
		}
		
		if (c.tag != "Tower" ||
		    c.isTrigger)
		{
			return;
		}
		
		TowerController controller = c.GetComponent<TowerController>();
		
		if (controller == null ||
		    towers == null)
			return;
		
		if (!towers.Contains(controller))
		{
			towers.Add(controller);
		}
	}
	
	void OnTriggerExit2D(Collider2D c)
	{
		if (c.gameObject == null)
			return;

		if (c.tag != "Tower")
		{
			return;
		}
		
		TowerController controller = c.GetComponent<TowerController>();

		if (controller == null ||
		    towers == null)
			return;

		if (towers.Contains(controller))
		{
			towers.Remove(controller);
		}
	}

	public enum State
	{
		Walking,
		Attacking,
	}
}
