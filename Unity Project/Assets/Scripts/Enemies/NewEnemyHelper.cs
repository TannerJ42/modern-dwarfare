﻿using UnityEngine;
using System.Collections;

public class NewEnemyHelper : MonoBehaviour 
{
    public NewEnemyController enemy;

	// Use this for initialization
	void Start() 
    {
	    
	}
	
	// Update is called once per frame
	void Update() 
    {
	    
	}

    public void UseAttack()
    {
        enemy.onUseAttack();
    }

    public void FinishAttacking()
    {
        enemy.onAttackFinish();
    }
}
