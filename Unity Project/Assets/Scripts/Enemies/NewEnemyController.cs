﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NewEnemyController : MonoBehaviour 
{
    // movement stuff
    public float speed = 2f;

    // pathing stuff
    Vector2[] waypoints;
    int waypointIndex;
    public float waypointDistance = 0.1f;
    public float waypointVariance = 0f;
    protected Vector2 currentWaypoint;

    // attacking stuff
    protected List<TowerController> towers;
    public float attackTime = 1f;
    float attackTimer;
    protected TowerController currentTarget;

    // animation stuff
    protected Animator anim;

    // state
    protected State state = State.Pathing;

    // hp stuff
    public int maxHP = 100;
    public int currentHP;
    public HealthBarController healthBar;
    
    // death stuff
    public int coinValue = 2;
    public int xpValue = 2;

    // xp stuff
    GameObject lastTowerToDamage;

    public bool isAlive
    {
        get
        {
            return currentHP > 0;
        }
    }

	// Use this for initialization
	void Start() 
    {
        // for now, start at the last waypoint.
        if (waypoints != null)
        {
            waypointIndex = waypoints.Length;
            currentWaypoint = waypoints[waypointIndex - 1];
        }
        
        // initialize attack timer
        attackTimer = attackTime;

        if (towers == null)
        {
            towers = new List<TowerController>();
        }

        if (anim == null)
        {
            anim = GetComponentInChildren<Animator>();
        }

        currentHP = maxHP;
	}
	
	// Update is called once per frame
	void Update() 
    {
        switch (state)
        {
            case State.Idle:
                CheckForAttack();
                break;
            case State.Pathing:
                CheckForAttack();
                break;
            case State.Attacking:
                if (currentTarget == null)
                {
                    state = State.Pathing;
                }
                break;
            default:
                break;
        }
	}

    void FixedUpdate()
    {
        switch (state)
        {
            case State.Idle:
                if (waypointIndex >= 0)
                {
                    anim.SetBool("Walking", true);
                    state = State.Pathing;
                }
                break;
            case State.Pathing:
                if (waypointIndex < 0)
                {
                    anim.SetBool("Walking", false);
                    state = State.Idle;
                    return;
                }
                if (Vector2.Distance(transform.position, currentWaypoint) < waypointDistance)
                {
                    this.onReachWaypoint();
                }
                else
                {
                    FaceTarget(currentWaypoint);
                    float stepSpeed = speed * Time.fixedDeltaTime;
                    Vector2 direction = Vector2.MoveTowards(transform.position, currentWaypoint, stepSpeed);
                    rigidbody2D.MovePosition(new Vector3(direction.x, direction.y, transform.position.z));
                }
                break;
            case State.Attacking:
                break;
            default:
                break;
        }
    }

    protected void CheckForAttack()
    {
        if (attackTimer > 0)
        {
            attackTimer -= Time.deltaTime;
        }

        if (attackTimer <= 0)
        {
            chooseTarget();
            if (currentTarget != null)
            {
                onAttackStart();
            }
        }
    }

    protected void FaceTarget(Vector2 faceTarget)
    {
        Vector2 direction = faceTarget - (Vector2)transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    public void setWaypoints(ref Vector2[] waypoints, int firstWaypoint)
    {
        this.waypoints = waypoints;
        this.waypointIndex = firstWaypoint;
    }

    protected virtual void onReachWaypoint()
    {
        waypointIndex -= 1;

        if (waypointIndex < 0)
        {
            return;
        }

        Vector2 randomPoint = Random.insideUnitCircle;
        randomPoint *= waypointVariance;

        currentWaypoint = waypoints[waypointIndex] + randomPoint;
    }

    protected virtual void onTowerEnterRange(TowerController tower)
    {
        if (towers == null)
        {
            towers = new List<TowerController>();
        }

        towers.Add(tower);
    }

    protected virtual void onTowerLeaveRange(TowerController tower)
    {
        if (towers == null)
        {
            towers = new List<TowerController>();
        }

        if (towers.Contains(tower))
        {
            towers.Remove(tower);
        }

        if (currentTarget == tower)
        {
            currentTarget = null;
        }
    }

    public virtual void chooseTarget()
    {
        float closest = float.MaxValue;

        for (int i = towers.Count - 1; i >= 0; i--)
        {
            if (towers[i] == null ||
                !towers[i].isAlive)
            {
                towers.RemoveAt(i);
                
            }
            else
            {
                float distance = Vector2.Distance(transform.position, towers[i].transform.position);
                if (distance < closest)
                {
                    closest = distance;
                    currentTarget = towers[i];
                }
            }
        }
    }

    public virtual void onAttackStart()
    {
        if (currentTarget == null)
        {
            state = State.Pathing;
            return;
        }
        state = State.Attacking;
        anim.SetTrigger("Attack");
        FaceTarget(currentTarget.transform.position);
    }

    public virtual void onAttackFinish()
    {
        currentTarget = null;
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.tag != "Tower" &&
            c.tag != "Base")
        {
            return;
        }

        if (!c.isTrigger)
        {
            onTowerEnterRange(c.gameObject.GetComponent<TowerController>());
        }
    }

    void OnTriggerExit2D(Collider2D c)
    {
        if (c.tag != "Tower" &&
            c.tag != "Base")
        {
            return;
        }

        if (!c.isTrigger)
        {
            onTowerLeaveRange(c.gameObject.GetComponent<TowerController>());
        }
    }

    public virtual void onUseAttack()
    {
        attackTimer = attackTime;
    }

    public virtual void TakeDamage(int damage, Vector2 location, GameObject attacker)
    {
        if (!isAlive)
        {
            return;
        }

        if (attacker && attacker.tag == "Tower")
        {
            lastTowerToDamage = attacker;
        }

        currentHP -= damage;

        if (healthBar != null)
        {
            healthBar.SetDamage(currentHP / (float)maxHP);
        }

        if (currentHP <= 0)
        {
            CoinEffectManager.AddCoinEffect((Vector2)transform.position, coinValue);
            Coins.AddCoins(coinValue);
            if (lastTowerToDamage != null)
            {
                TowerController tc = lastTowerToDamage.GetComponent<TowerController>();
                if (tc != null)
                {
                    tc.GainXP(this.xpValue);
                }
            }
            Destroy(this.gameObject);
        }
    }

    public virtual void Die()
    {
        Destroy(this.gameObject);
    }

    public enum State
    {
        Pathing,
        Idle,
        Attacking,
    }

    void OnCollisionEnter2D(Collision2D c)
    {
        if (c.gameObject == null)
        {
            return;
        }

        if (c.gameObject.tag == "Enemy")
        {
            Physics2D.IgnoreCollision(this.collider2D, c.gameObject.collider2D);
        }
    }
}
