﻿using UnityEngine;
using System.Collections;

public class WargController : NewEnemyController {

    public int attackDamage = 100;

    public override void onAttackStart()
    {
        if (currentTarget == null)
        {
            state = State.Pathing;
            return;
        }
        base.onAttackStart();
    }

    public override void onUseAttack()
    {
        if (currentTarget == null)
        {
            onAttackFinish();
            return;
        }

        currentTarget.takeDamage(attackDamage, transform.position);

        base.onUseAttack();
    }

    public override void onAttackFinish()
    {
        base.onAttackFinish();
    }

    protected override void onTowerEnterRange(TowerController tower)
    {
        Debug.Log("Entering range.");

        if (tower.name == "Base")
        {
            base.onTowerEnterRange(tower);
        }
    }

    protected override void onTowerLeaveRange(TowerController tower)
    {
        if (tower.name == "Base")
        {
            base.onTowerLeaveRange(tower);
        }
    }
}
