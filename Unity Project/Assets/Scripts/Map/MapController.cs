﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapController : MonoBehaviour 
{
	Zones currentZone;

	public GameObject[] maps;
	public static GameObject screenSprite;

	GameObject[] baseTowers;

	bool faded = false;

	// Use this for initialization
	void Start() 
	{
		screenSprite = GameObject.Find ("ScreenFade");

        GameObject[] towers = GameObject.FindGameObjectsWithTag("Tower");

        List<GameObject> bases = new List<GameObject>();

        for (int i = 0; i <= towers.Length - 1; i++)
        {
            GameObject tc = towers[i];
            if (tc.name == "Base")
            {
                bases.Add(tc);
            }
        }

        baseTowers = bases.ToArray();
	}
	
	// Update is called once per frame
	void Update() 
	{
		if (isABaseDead() &&
		    !faded)
		{
			faded = true;
			FadeToBlack();
		}
	}

	public void CompleteZone()
	{
		if ((int)currentZone > maps.Length - 1)
		{
			return;
		}

		Animator anim = maps[(int)currentZone].GetComponent<Animator>();

		anim.SetTrigger("Finish");

		currentZone += 1;
	}

    public bool isABaseDead()
    {
        for (int i = 0; i <= baseTowers.Length - 1; i++)
        {
            if (baseTowers[i] == null)
            {
                return true;
            }
        }

        return false;
    }

    static void FadeToBlack()
	{
		Animator anim = screenSprite.GetComponent<Animator>();
		ScreenFaderController.Lose ();
		anim.SetTrigger("Lose");
	}

	public enum Zones
	{
		One = 0,
		Two = 1,
		Three = 2,
		Four = 3
	}
}
