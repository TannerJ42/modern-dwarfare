﻿using UnityEngine;
using System.Collections;

public class WaypointController : MonoBehaviour 
{
	public WaypointController nextWaypoint;
	Vector2 coordinates;

	// Use this for initialization
	void Start()
	{
		this.coordinates = transform.position;
	}
	
	// Update is called once per frame
	void Update() 
	{
		
	}

	public void SetWaypoint(WaypointController waypoint)
	{
		nextWaypoint = waypoint;
	}

	public WaypointController GetNextWaypoint()
	{
		if (!nextWaypoint)
			return null;
		return nextWaypoint;
	}

	public Vector2 GetCoordinates()
	{
		return coordinates;
	}
}
