﻿using UnityEngine;
using System.Collections;

public class MapBorderController : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void Disable()
	{
		Destroy(this.gameObject);
	}

	void OnCollisionEnter2D(Collision2D c)
	{
		if (c.gameObject.tag != "Player")
		{
			Physics2D.IgnoreCollision(c.collider, this.collider2D);
		}
	}
}
