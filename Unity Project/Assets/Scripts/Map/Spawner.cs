﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spawner : MonoBehaviour 
{
	bool waveStarted;

	float timer = 0;
	public float delay = 1f;
	public float interval = 3f;
	public GameObject[] spawns;
	int numberOfSpawns = 0;

	//public GameObject enemyToSpawn;
	public WaypointController firstWaypoint;

    Vector2[] waypoints;

	Transform myTransform;

	// Use this for initialization
	void Start() 
	{
		myTransform = gameObject.transform;	
		timer = delay;

        List<Vector2> waypointList = new List<Vector2>();

        WaypointController wp = firstWaypoint;

        while (wp != null)
        {
            waypointList.Add((Vector2)wp.gameObject.transform.position);
            wp = wp.GetNextWaypoint();
        }

        Debug.Log("Found " + waypointList.Count + " waypoints.");

        waypointList.Reverse();

        waypoints = waypointList.ToArray();
	}

    // Update is called once per frame
    void Update() 
	{
		
	}

	void FixedUpdate()
	{
		timer -= Time.fixedDeltaTime;

		if (numberOfSpawns == spawns.Length)
		{
			Destroy(this.gameObject);
		}


		if (timer <= 0)
		{
			SpawnEnemy();
			timer = interval;
		}
	}

	void SpawnEnemy()
	{
		if (firstWaypoint == null ||
		    numberOfSpawns > spawns.Length )
		{
			return;
		}

		GameObject enemy = (GameObject)Instantiate(spawns[numberOfSpawns]);
		numberOfSpawns += 1;

        NewEnemyController ec = enemy.GetComponent<NewEnemyController>();
		if (ec == null)
		{
			Destroy (enemy);
			return;
		}

		Vector3 location = transform.position;
		location.z = enemy.transform.position.z;
		enemy.transform.position = location;

        ec.setWaypoints(ref waypoints, waypoints.Length - 1);

//		GameObject enemyParent = (GameObject)Instantiate(spawns[numberOfSpawns]);
//
//		if (enemyParent.transform.childCount > 0)
//		{
//			Vector3 newPosition = myTransform.position;
//			newPosition.z = enemyParent.transform.position.z;
//
//			enemyParent.transform.position = newPosition;
//
//			for(int i = enemyParent.transform.childCount - 1; i >= 0; i--)
//			{
//				EnemyController controller = enemyParent.transform.GetChild(i).GetComponent<EnemyController>();
//				controller.currentWaypoint = firstWaypoint;
//				enemyParent.transform.GetChild(i).parent = null;
//			}
//		}

//		GameObject.Destroy(enemyParent);
	}
}
