﻿using UnityEngine;
using System.Collections;

public class NodeController : MonoBehaviour
{
	GameObject towerMenu;

	public GameObject buildingAnimPrefab;

	public GameObject towerMenuLocation;

	public GameObject Tower1Prefab; // machine gun
	public GameObject Tower2Prefab; // cannon
	public GameObject Tower3Prefab; // sniper
    public GameObject Tower4Prefab; // quake

	bool Occupied = false;
	bool TowerBuilt = false;

	GameObject tower = null;

	// Use this for initialization
	void Start() 
	{
		towerMenu = GameObject.Find("Build Icons");
	}

	void Update()
	{
		if (Occupied &&
		    !TowerBuilt)
		{
			if (Input.GetKeyDown (KeyCode.Alpha1))
			{
				if (Coins.Amount >= Coins.Tower1Cost)
				{
					Build(Tower1Prefab);
					Coins.Amount -= Coins.Tower1Cost;
				}
			}
			else if (Input.GetKeyDown (KeyCode.Alpha2))
			{
				if (Coins.Amount >= Coins.Tower2Cost)
				{
					Build(Tower2Prefab);
					Coins.Amount -= Coins.Tower2Cost;
				}
			}
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                if (Coins.Amount >= Coins.Tower3Cost)
                {
                    Build(Tower3Prefab);
                    Coins.Amount -= Coins.Tower3Cost;
                }
            }
            else if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                if (Coins.Amount >= Coins.Tower4Cost)
                {
                    Build(Tower4Prefab);
                    Coins.Amount -= Coins.Tower4Cost;
                }
            }
		}

		if (TowerBuilt &&
		    tower == null)
		{
			TowerBuilt = false;
			this.collider2D.enabled = true;

		}
	}

	void OnTriggerEnter2D(Collider2D c)
	{
		if (c.tag != "Player" ||
		    c.isTrigger)
		{
			Physics2D.IgnoreCollision(c.collider2D, transform.collider2D);
			return;
		}

		Occupied = true;

		if (towerMenu != null)
		{
			towerMenu.SetActive(true);
			towerMenu.transform.position = towerMenuLocation.transform.position;
		}
	}

	void OnTriggerExit2D(Collider2D c)
	{
		if (c.tag != "Player" ||
		    c.isTrigger)
		{
			Physics2D.IgnoreCollision(c.collider2D, transform.collider2D);
			return;
		}

		Occupied = false;

		if (towerMenu != null)
		{
			towerMenu.SetActive(false);
		}
	}

	void Build(GameObject prefab)
	{
		GameObject buildingAnimation = (GameObject)Instantiate(buildingAnimPrefab, this.transform.position, this.transform.rotation);
		tower = (GameObject)Instantiate(prefab, this.transform.position, this.transform.rotation);
		this.collider2D.enabled = false;
		Occupied = false;
		towerMenu.SetActive(false);
		TowerBuilt = true;

		BuildingAnimationManager manager = buildingAnimation.GetComponentInChildren<BuildingAnimationManager>();
		manager.building = tower;
		tower.SetActive(false);
	}
}
